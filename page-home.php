<?php
/**
* Template Name: Home
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header();

?>

<body class="home page-template-default page page-id-6 wpb-js-composer js-comp-ver-4.7.2a vc_responsive  ut-tbs3-default ut-layout-full-width-layout-home" data-gr-c-s-loaded="true">
<div class="clear"></div>
<header class="headwrapper">
<div class="ult-wrapper wrapper  hidden-phone hidden-tablet" id="wrapper-1">

<div class="ult-container  container " id="container-1">
<div class="row">
		<div class="ult-column col-md-12 " id="col-1-1">
			<div class="colwrapper"><div class="widget widget_ultimatumwysiwyg inner-container"><div id="jumbotron2-11" class="col span_2_of_3">
<h2 class="jumbotron-heading sub-header-font"><span class="jumbotron-heading-inner">Drywall Experts You can Count on</span></h2>
</div>
<div class="col span_1_of_3" style="float: right; margin-top: 5px;">
<div style="float: right; text-align: center; width: 100%; max-width: 300px; min-width: 280px;"><a id="button-id-1" class="button button-34 button-block" href="tel:6046142486"><span class="button-inner"><span class="button-text-wrap"><span class="button-text sub-header-font" style="margin-bottom: -5px;"><span style="font-size: 24px">Tel: 604-614-2486</span></span></span></span></a></div>
</div>
</div>
</div>
</div></div>
		</div>
		</div>

<div class="ult-wrapper wrapper  hidden-phone hidden-tablet" id="wrapper-4">

<div class="ult-container  container " id="container-4">
<div class="row">
		<div class="ult-column col-md-4 " id="col-4-1">
			<div class="colwrapper"><div id="logo-container"><h2><a href="<?php echo get_site_url();?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ocean-drywall-new.png" alt="Drywall Contactors - Ocean Drywall & Renovation" class="img-responsive"></a></h2></div></div>
		</div>
		<div class="ult-column col-md-8 " id="col-4-2">
			<div class="colwrapper"><div class="widget widget_ultimatummenu inner-container"><script type="text/javascript">
	//<![CDATA[
	jQuery(document).ready(function() {
		for (i=0;i<1;i++) {
			setTimeout('addDot()',1000);
			}
		});
		function addDot() {
			jQuery(document).ready(function($) {
				jQuery('#ultimatummenu-3-item .menu').wfMegaMenu({
					rowItems: 1,
					speed: 'normal',
					effect: 'slide',
					subMenuWidth:200
				});
			});
	}
	//]]>
</script>
<div class="ultimatum-nav">

    <div class="wfm-mega-menu" id="ultimatummenu-3-item" style="width:500px;float:right">
        <ul id="menu-main-menu" class="menu"><li id="menu-item-326" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-326"><a href="<?php echo get_site_url();?>">HOME</a></li>
            <li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="<?php echo get_site_url();?>/about ">ABOUT US</a></li>
            <li id="menu-item-41" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a href="<?php echo get_site_url();?>/services">SERVICES</a></li>
            <li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a href="<?php echo get_site_url();?>/contact-page">CONTACT US</a></li>
        </ul>
    </div>

</div>

</div><div class="clearfix"></div></div>
		</div>
	</div></div>
</div>
<div class="ult-wrapper wrapper  hidden-desktop" id="wrapper-32">

<div class="ult-container  container " id="container-32">
<div class="row">
		<div class="ult-column col-md-9 " id="col-32-1">
			<div class="colwrapper"><div class="widget widget_ultimatummenu inner-container"><nav class="navbar navbar-inverse" role="navigation">
	<div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-ultimatummenu-7">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                                                <a class="navbar-brand" href="<?php echo get_site_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ocean-drywall-new.png" title="Drywall Contactors - Ocean Drywall & Renovation"></a>
                                    </div>
        <div class="collapse navbar-collapse" id="bs-navbar-collapse-ultimatummenu-7">
            <ul id="menu-main-menu-1" class="nav navbar-nav"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-326 active"><a title="HOME" href="<?php echo get_site_url();?>">HOME</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a title="ABOUT US" href="<?php echo get_site_url();?>/about">ABOUT US</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a title="SERVICES" href="<?php echo get_site_url();?>/services">SERVICES</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a title="CONTACT US" href="<?php echo get_site_url();?>/contact-page">CONTACT US</a></li>
</ul>                    </div>
	</div>
</nav>
</div><div class="clearfix"></div></div>
		</div>
		<div class="ult-column col-md-3 " id="col-32-2">
			<div class="colwrapper"><div class="widget widget_ultimatumwysiwyg inner-container"><div class="alignright"><a href="tel:6046142486"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone.png" alt="phone" width="25" height="25" class="size-full wp-image-211"></a></div>
</div></div>
		</div>
		</div></div>
</div>
</header>
<div class="bodywrapper" id="bodywrapper">
<div class="ult-wrapper wrapper " id="wrapper-8">

<div class="ult-container  container " id="container-8">
<div class="row">
		<div class="ult-column col-md-12 " id="col-8-1">
			<div class="colwrapper"><div class="widget widget_ultimatumpcontent inner-container"><div style="margin-top: 80px;"></div>
<div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-parallax="1.5" class="vc_row wpb_row vc_row-fluid vc_custom_1443827758558 vc_general vc_parallax vc_parallax-content-moving" style="position: relative; left: -27.5px; box-sizing: border-box; width: 1265px; padding-left: 27.5px; padding-right: 27.5px;"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper"><h2 style="color: #1da1f2;text-align: left" class="vc_custom_heading vc_custom_1443817332265">Reliable, Affordable, Reasonable</h2><h2 style="color: #ffffff;text-align: left" class="vc_custom_heading vc_custom_1443817456665">DRYWALL COMPANY</h2><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper"><p></p>
<div class="vc_btn3-container vc_btn3-left"><a class="lbp-inline-link-1 btn vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-warning" title="" href="<?php echo get_site_url();?>/contact-page" target="_self"><b><span style="font-size: 20px">REQUEST ESTIMATE &gt;&gt;&gt;</span></b></a></div>
<p></p></div></div></div></div></div><div class="vc_parallax-inner skrollable skrollable-between" data-bottom-top="top: -50%;" data-top-bottom="top: 0%;" style="height: 150%; top: -31.5228%;"></div></div><div class="vc_row-full-width"></div>
</div></div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper  hidden-phone hidden-tablet hidden-desktop" id="wrapper-2">

<div class="ult-container  container " id="container-2">
<div class="row">
		<div class="ult-column col-md-12 " id="col-2-1">
			<div class="colwrapper">&nbsp;</div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper " id="wrapper-5">
			<div class="29 " id="col-5-1">
			<div class="colwrapper"><div class="widget widget_ultimatumwysiwyg inner-container"><div id="jumbotron2-27" class="uxi-widget" data-column="1" data-row="161">
<div class="content">
<div class="jumbotron">
<h1 class="jumbotron-heading header-font" style="text-align: center;"><span class="jumbotron-heading-inner2">Professional Drywall Contractors</span></h1>
<h2 class="jumbotron-subheading sub-header-font" style="text-align: center;"><span class="jumbotron-subheading-inner"> No Job Is Too Small! </span></h2>
<div class="jumbotron-body aligncenter" style="max-width: 900px; margin-left: auto; margin-right: auto;">
<p class="jumbotron-paragraph body-font" style="text-align: center;"><span class="jumbotron-paragraph-inner">Ocean Drywall & Renovation offers a full spectrum of services for residential, commercial, and industrial drywall applications including acoustical t-bar ceilings, steel stud framing, drywall installation, drywall taping &amp; finishing, textured ceiling repairs &amp; finishes.</span></p>
</div>
<div class="aligncenter" style="width: 100%; max-width: 1100px; margin-left: auto; margin-right: auto;">
<div class="section group jumbotron2">
<div class="col span_1_of_3">
<div class="wrap wrap-210" data-column="1" data-id="210">
<div class="uxi-container">
<div class="container-inner">
<div class="uxi-row">
<div class="grid-tab-12">
<div class="grid-inner">
<div id="uxi_widget_cta2-81" class="uxi-widget" data-column="1" data-row="210">
<div class="content">
<div class="cta2">
<div class="cta2-heading-wrap">
<div class="cta2-heading">
<h2 class="sub-header-font"><a href="<?php echo get_site_url();?>/services"><img class="alignnone wp-image-64" src="<?php echo get_template_directory_uri(); ?>/assets/img/langleyCTA1.jpg" alt="langleyCTA1" width="359" height="220"></a></h2>
<h2 class="sub-header-font" style="text-align: center;">Commercial</h2>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="wrap wrap-211" data-column="1" data-id="211">
<div class="uxi-container">
<div class="container-inner">
<div class="uxi-row">
<div class="grid-tab-12">
<div class="grid-inner">
<div id="uxi_widget_wysiwyg_text_area-26" class="uxi-widget" data-column="1" data-row="211">
<div class="content jumbotron2">
<p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Steel Stud Application<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Insulation, Vapor Barrier &amp; Accoustical Sealant<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Drywall &amp; Taping<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Suspended Ceilings (Drywall &amp; T-Bar)<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Fire Rating</p>
<hr>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col span_1_of_3">
<div class="wrap wrap-212" data-column="2" data-id="212">
<div class="uxi-container">
<div class="container-inner">
<div class="uxi-row">
<div class="grid-tab-12">
<div class="grid-inner">
<div id="uxi_widget_cta2-79" class="uxi-widget" data-column="1" data-row="212">
<div class="content">
<div class="cta2">
<div class="cta2-heading-wrap">
<div class="cta2-heading">
<h2 class="sub-header-font"><a href="<?php echo get_site_url();?>/services"><img class="alignnone wp-image-65" src="<?php echo get_template_directory_uri(); ?>/assets/img/langleyCTA2.jpg" alt="langleyCTA2" width="359" height="220"></a></h2>
<h2 class="sub-header-font" style="text-align: center;">Residential</h2>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="wrap wrap-213" data-column="2" data-id="213">
<div class="uxi-container">
<div class="container-inner">
<div class="uxi-row">
<div class="grid-tab-12">
<div class="grid-inner">
<div id="uxi_widget_wysiwyg_text_area-27" class="uxi-widget" data-column="1" data-row="213">
<div class="content jumbotron2">
<p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Insulation, Vapor Barrier &amp; Accoustical Sealant<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Drywall &amp; Taping<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Textured Ceilings<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Suspended T-Bar Ceiling<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Painting</p>
<hr>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col span_1_of_3">
<div class="wrap wrap-214" data-column="3" data-id="214">
<div class="uxi-container">
<div class="container-inner">
<div class="uxi-row">
<div class="grid-tab-12">
<div class="grid-inner">
<div id="uxi_widget_cta2-80" class="uxi-widget" data-column="1" data-row="214">
<div class="content">
<div class="cta2">
<div class="cta2-heading-wrap">
<div class="cta2-heading">
<h2 class="sub-header-font"><a href="<?php echo get_site_url();?>/services"><img class="alignnone wp-image-66" src="<?php echo get_template_directory_uri(); ?>/assets/img/langleyCTA3.jpg" alt="langleyCTA3" width="359" height="220"></a></h2>
<h2 class="sub-header-font" style="text-align: center;">Renovation &amp; Repairs</h2>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="wrap wrap-215" data-column="3" data-id="215">
<div class="uxi-container">
<div class="container-inner">
<div class="uxi-row">
<div class="grid-tab-12">
<div class="grid-inner">
<div id="uxi_widget_wysiwyg_text_area-25" class="uxi-widget" data-column="1" data-row="215">
<div class="content jumbotron2">
<p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Water Damage<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Textured Blend Matches<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Sound Proofing<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Painting<br>
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/blue-checkmark.png" alt=""> Textured Removal</p>
<hr>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div></div>
			</div>

				</div>
<div class="ult-wrapper wrapper " id="wrapper-7">
			<div class="29 " id="col-7-1">
			<div class="colwrapper"><div class="hidden-phone inner-container"> 	<div id="content" role="main">
 	 	 	<article id="post-6" class="post-6 page type-page status-publish hentry entry post-inner">

 		 		
 		 		<div class="entry-content">
 			<div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-parallax="1.5" data-vc-parallax-image="assets/img/langleyBG2.jpg" class="vc_bg vc_row wpb_row vc_row-fluid vc_row-o-full-height vc_row-o-content-middle vc_general vc_parallax vc_parallax-content-moving" style="position: relative; left: 15px; box-sizing: border-box; width: 1265px; padding-left: 0px; padding-right: 45px;"><div class="vc_row-full-height-fixer" style="height: 666px;"><!-- IE flexbox min height vertical align fixer --></div><div class="vc_row-full-height-fixer" style="height: 666px;"><!-- IE flexbox min height vertical align fixer --></div><div class="vc_row-full-height-fixer" style="height: 662px;"><!-- IE flexbox min height vertical align fixer --></div><div class="vc_row-full-height-fixer" style="height: 662px;"><!-- IE flexbox min height vertical align fixer --></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="wpb_wrapper"></div></div><div class="wpb_column vc_column_container vc_col-sm-8"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element">
		<div class="wpb_wrapper">
			<div style="width: 100%;">
<div style="max-width: 800px; border: 1px solid #eeeeee; padding: 10px; margin: 20px; color: #fff; background-color: rgba(0,0,0,0.7);">
<h2 class="vc_custom_heading vc_custom_1443817456665" style="font-size: 86px; color: #ffffff; text-align: center;">CLEAN &amp; AFFORDABLE</h2>
<h2 class="vc_custom_heading vc_custom_1443817332265" style="font-size: 30px; color: #1da1f2; text-align: center; margin-top: -10px;">Drywall Contractors</h2>
<p style="text-align: center;">We understand the precision, care, and dedication that is required from your drywall professionals, and we have the knowledge, experience and passion for perfection that allows us to perform commercial and residential drywall installations or repairs flawlessly. Whether you are doing renovations, additions, repairs, or new designs, our team will go the distance in order to give you a kind of quality that stands apart from the rest-we’ll even work with your insurance agency if applicable. Contact us today to learn more!</p>
<p style="text-align: center;">For years, we have been developing the skill and knowledge necessary to install and repair drywall with perfection. With our wide range of drywall services, you can now get the level of excellence you need for your home, your office, or your industrial space. From textured ceilings to drywall board installations, we can do it all at a price you’ll love.</p>
<p style="text-align: center;">Our team is passionate about what they do, and they are ready to provide you with a service that will give you results that last. Friendly, professional and licensed, we are focused on our customers’ drywall needs, budget, and space so that we can give them a product that exceeds expectations for years to come.</p>
<p style="text-align: center;"><a href="<?php echo get_site_url();?>/contact-page"><span style="color: #8e9639; font-weight: bold; text-decoration: underline;">Schedule your free quote</span></a> today and see how our <strong>drywall contractors</strong> can help you!</p>
</div>
<div style="max-width: 800px; padding: color: #fff; padding: 0px; margin: 20px;">
<div class="section group aligncenter" style="margin-left: auto; margin-right: auto;">
<div class="col span_1_of_2">
<div style="text-align: center; width: 100%;"><a id="button-id-1" class="lbp-inline-link-1 button button-34 button-block" href="<?php echo get_site_url();?>/contact-page" target="_self"><span class="button-inner"><span class="button-text-wrap"><span class="button-text sub-header-font" style="font-size: 18px;">Request Estimate</span></span></span></a></div>
</div>
<div class="col span_1_of_2">
<div style="text-align: center; width: 100%;"><a id="button-id-1" class="button button-34 button-block" href="<?php echo get_site_url();?>/services"><span class="button-inner"><span class="button-text-wrap"><span class="button-text sub-header-font" style="font-size: 18px;">Drywall Services</span></span></span></a></div>
</div>
</div>
</div>
</div>

		</div>
	</div>
</div></div><div class="vc_parallax-inner skrollable skrollable-before" data-bottom-top="top: -50%;" data-top-bottom="top: 0%;" style="height: 150%; background-image: url(&quot;images/langleyBG2.jpg&quot;); top: -50%;"></div></div><div class="vc_row-full-width"></div>
<div style="display: none;">
<div id="lbp-inline-href-1" style="padding: 10px; background: #fff;">
<h3 style="text-align: center; font-weight: bold; color: #ff9000;"> Get Your FREE ESTIMATE Today! </h3>
<div role="form" class="wpcf7" id="wpcf7-f227-p6-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="<?php echo get_site_url();?>#wpcf7-f227-p6-o1" method="post" class="wpcf7-form cf7-style twenty-fifteen-pattern" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="227">
<input type="hidden" name="_wpcf7_version" value="4.7">
<input type="hidden" name="_wpcf7_locale" value="en_US">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f227-p6-o1">
<input type="hidden" name="_wpnonce" value="5e8a314022">
</div>
<p>Your Name (required)<br>
    <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </p>
<p>Your Email (required)<br>
    <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </p>
<p>Your Phone# (required)<br>
    <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span> </p>
<p>Describe Your Project<br>
    <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </p>
<p><input type="submit" value="Send Request" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span><span class="ajax-loader"></span></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div>
</div>
</div>
 		</article></div>
 		 		<div class="clearfix"></div>
 	
 			<div class="clearfix"></div><div style="clear:both"></div> 	</div>
 	</div><div class="widget widget_ultimatumwysiwyg inner-container"><div style="clear:both"></div>
<div class="aligncenter" style="width: 100%; max-width: 1300px; margin-left: auto; margin-right: auto;">
<div class="section group aligncenter" style="max-width: 1250px; margin-left: auto; margin-right: auto;">
<div class="col span_1_of_2">
<div id="uxi_widget_cta2-75" class="uxi-widget" data-column="1" data-row="97">
<div class="content">
<div class="cta2">
<div class="cta2-heading-wrap">
<div class="cta2-image-wrap "><img class="cta2-image img-rounded" src="<?php echo get_template_directory_uri();; ?>/assets/img/langleyLongCTA1.jpg" alt="langleyLongCTA1" width="95%" height="auto"></div>
<div class="cta2-heading">
<h2 class="sub-header-font">Water Damage</h2>
</div>
</div>
<p class="cta2-paragraph">Water damage can be caused by an overflowing bathtub, a broken hose to the washing machine, a leaking pipe inside a wall, any of these issues can create catastrophic water damage to the drywall in your home. For water damaged drywall repair, email or call Ocean Drywall & Renovation at 604-614-2486.</p>
</div>
</div>
</div>
</div>
<div class="col span_1_of_2">
<div id="uxi_widget_cta2-65" class="uxi-widget" data-column="2" data-row="97">
<div class="content">
<div class="cta2">
<div class="cta2-heading-wrap">
<div class="cta2-image-wrap "><img class="cta2-image img-rounded" src="<?php echo get_template_directory_uri(); ?>/assets/img/texture_removal.png" alt="texture_removal" width="95%" height="auto"></div>
<div class="cta2-heading">
<h2 class="sub-header-font">Texture Removal</h2>
</div>
</div>
<p class="cta2-paragraph">When opting for removing your textured ceilings, you will see a complete transformation of the rooms. Before, your ceilings added an old-fashioned look upon your home, but after your ceilings look new, modern, and above all clean. The difference is amazing and most of our clients immediately fall in love with their new ceilings.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div></div>
			</div>

<div class="sidebar-instagram">
	<?php get_sidebar(); ?>		
</div>


<footer class="footwrapper">
<div class="ult-wrapper wrapper " id="wrapper-11">

<div class="ult-container  container " id="container-11">
<div class="row">
		<div class="ult-column col-md-12 " id="col-11-1">
			<div class="colwrapper"><div class="widget widget_ultimatummenu inner-container"><div class="ultimatum-menu-container" data-menureplacer="480"><div class="ultimatum-regular-menu"><script type="text/javascript">
	//<![CDATA[
	jQuery(document).ready(function() {
		for (i=0;i<1;i++) {
			setTimeout('addDot()',1000);
			}
		});
		function addDot() {
			jQuery(document).ready(function($) {
				jQuery('#ultimatummenu-5-item .menu').wfMegaMenu({
					rowItems: 1,
					speed: 'normal',
					effect: 'slide',
					subMenuWidth:200
				});
			});
	}
	//]]>
</script>
<div class="ultimatum-nav">
<div class="wfm-mega-menu" id="ultimatummenu-5-item" style="width:1170px;">
<ul id="menu-footer-menu" class="menu"><li id="menu-item-325" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-325"><a href="<?php echo get_site_url();?>">Home</a></li>
<li id="menu-item-158" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-9 current_page_item menu-item-158"><a href="<?php echo get_site_url();?>/about">About Us</a></li>
<li id="menu-item-157" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157"><a href="<?php echo get_site_url();?>/contact-page">Contact Us</a></li>
<li id="menu-item-155" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-155"><a href="<?php echo get_site_url();?>/services">Services</a></li>
</ul></div>
</div>
</div><style>
    #ultimatummenu-5-responsive-menu .slicknav_btn {float:left;?>}
</style>
<div style="display:none">
    <ul id="ultimatummenu-5-resonsive" class="menu"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-325"><a href="<?php echo get_site_url();?>">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="<?php echo get_site_url();?>/about">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157"><a href="<?php echo get_site_url();?>/contact-page">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-155"><a href="<?php echo get_site_url();?>/services">Services</a></li>
</ul></div>
<div id="ultimatummenu-5-responsive-menu" class="ultimatum-responsive-menu" style="display: none;"><div class="slicknav_menu" style="display: none;"><a href="<?php echo get_template_directory_uri(); ?>/#" aria-haspopup="true" tabindex="0" class="slicknav_btn slicknav_collapsed" style="outline: none;"><span class="slicknav_menutxt">Menu</span><span class="slicknav_icon"><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span></span></a><ul class="slicknav_nav slicknav_hidden" aria-hidden="true" role="menu" style="display: none;"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-325"><a href="<?php echo get_site_url();?>" role="menuitem" tabindex="-1">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="<?php echo get_site_url();?>/about" role="menuitem" tabindex="-1">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157"><a href="<?php echo get_site_url();?>/contact-page" role="menuitem" tabindex="-1">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-155"><a href="<?php echo get_site_url();?>/services" role="menuitem" tabindex="-1">Services</a></li>
</ul></div>
 <div class="slicknav_menu" style="display: none;"></div>
</div>
<script type="text/javascript">
	//<![CDATA[
	jQuery(document).ready(function() {
	    jQuery('#ultimatummenu-5-resonsive').slicknav({
            label:'Menu',
            allowParentLinks: true,            prependTo:'#ultimatummenu-5-responsive-menu'
        });
	});
//]]>
</script></div></div><div class="clearfix"></div></div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper  hidden-phone hidden-tablet hidden-desktop" id="wrapper-12">

<div class="ult-container  container " id="container-12">
<div class="row">
		<div class="ult-column col-md-12 " id="col-12-1">
			<div class="colwrapper">&nbsp;</div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper " id="wrapper-34">

<div class="ult-container  container " id="container-34">
<div class="row">
		<div class="ult-column col-md-12 " id="col-34-1">
			<div class="colwrapper"><div class="widget widget_ultimatumwysiwyg inner-container"><div class="section group">
<div class="col span_1_of_3">
<div id="uxi_widget_wysiwyg_text_area-10" class="uxi-widget" data-column="1" data-row="22">
<div class="content">
<p><strong><span style="font-size:16px; color:#8b8e91;">CALL US:</span> <span style="color:#ffffff; font-size:22px;">604-614-2486</span></strong><br>
<strong><span style="font-size:16px; color:#8b8e91;">TOP RATED DRYWALL COMPANY</span></strong></p>
</div>
</div>
</div>
<div class="col span_1_of_3" style=" text-align: center;"><span style="color:#ffffff;">© 2019 Ocean Drywall. All Rights Reserved.</span></div>
<div class="col span_1_of_3">
<div style="float: right; text-align: center; width: 100%; max-width: 300px;"><a id="button-id-1" class="button button-34 button-block" href="<?php echo get_site_url();?>/contact-page"><span class="button-inner"><span class="button-text-wrap"><span class="button-text sub-header-font">FREE PROJECT QUOTE</span></span></span></a></div>
</div>
</div>
</div></div>
		</div>
		</div></div>
</div>
</footer>


<div id="cboxOverlay" style="display: none;"></div>
<div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;">
    <div id="cboxWrapper">
        <div>
            <div id="cboxTopLeft" style="float: left;"></div>
            <div id="cboxTopCenter" style="float: left;"></div>
            <div id="cboxTopRight" style="float: left;"></div>
        </div>
        <div style="clear: left;">
            <div id="cboxMiddleLeft" style="float: left;"></div>
            <div id="cboxContent" style="float: left;">
                <div id="cboxTitle" style="float: left;"></div>
                <div id="cboxCurrent" style="float: left;"></div><button type="button"
                    id="cboxPrevious"></button><button type="button" id="cboxNext"></button><button
                    id="cboxSlideshow"></button>
                <div id="cboxLoadingOverlay" style="float: left;"></div>
                <div id="cboxLoadingGraphic" style="float: left;"></div>
            </div>
            <div id="cboxMiddleRight" style="float: left;"></div>
        </div>
        <div style="clear: left;">
            <div id="cboxBottomLeft" style="float: left;"></div>
            <div id="cboxBottomCenter" style="float: left;"></div>
            <div id="cboxBottomRight" style="float: left;"></div>
        </div>
    </div>
    <div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div>
</div>
<div id="cboxOverlay" style="display: none;"></div>
<div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;">
    <div id="cboxWrapper">
        <div>
            <div id="cboxTopLeft" style="float: left;"></div>
            <div id="cboxTopCenter" style="float: left;"></div>
            <div id="cboxTopRight" style="float: left;"></div>
        </div>
        <div style="clear: left;">
            <div id="cboxMiddleLeft" style="float: left;"></div>
            <div id="cboxContent" style="float: left;">
                <div id="cboxTitle" style="float: left;"></div>
                <div id="cboxCurrent" style="float: left;"></div><button type="button"
                    id="cboxPrevious"></button><button type="button" id="cboxNext"></button><button
                    id="cboxSlideshow"></button>
                <div id="cboxLoadingOverlay" style="float: left;"></div>
                <div id="cboxLoadingGraphic" style="float: left;"></div>
            </div>
            <div id="cboxMiddleRight" style="float: left;"></div>
        </div>
        <div style="clear: left;">
            <div id="cboxBottomLeft" style="float: left;"></div>
            <div id="cboxBottomCenter" style="float: left;"></div>
            <div id="cboxBottomRight" style="float: left;"></div>
        </div>
    </div>
    <div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div>
</div>


</body>

<?php
    get_footer();
?>
