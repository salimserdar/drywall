<?php
/**
* Template Name: Contact
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header();

?>

<body class="page-template-default page page-id-39 wpb-js-composer js-comp-ver-4.7.2a vc_responsive  ut-tbs3-default ut-layout-full-width-layout-page" data-gr-c-s-loaded="true">
<div class="clear"></div>

<header class="headwrapper">
	<div class="ult-wrapper wrapper  hidden-phone hidden-tablet" id="wrapper-1">

	<div class="ult-container  container " id="container-1">
	<div class="row">
			<div class="ult-column col-md-12 " id="col-1-1">
				<div class="colwrapper"><div class="widget widget_ultimatumwysiwyg inner-container"><div id="jumbotron2-11" class="col span_2_of_3">
	<h2 class="jumbotron-heading sub-header-font"><span class="jumbotron-heading-inner">Drywall Experts You can Count on</span></h2>
	</div>
	<div class="col span_1_of_3" style="float: right; margin-top: 5px;">
	<div style="float: right; text-align: center; width: 100%; max-width: 300px; min-width: 280px;"><a id="button-id-1" class="button button-34 button-block" href="tel:6046142486"><span class="button-inner"><span class="button-text-wrap"><span class="button-text sub-header-font" style="margin-bottom: -5px;"><span style="font-size: 24px">Tel: 604-614-2486</span></span></span></span></a></div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>

	<div class="ult-wrapper wrapper  hidden-phone hidden-tablet" id="wrapper-4">

	<div class="ult-container  container " id="container-4">
	<div class="row">
			<div class="ult-column col-md-4 " id="col-4-1">
				<div class="colwrapper"><div id="logo-container"><h2><a href="<?php echo get_site_url();?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ocean-drywall-new.png" alt="Drywall Contactors - Ocean Drywall & Renovation" class="img-responsive"></a></h2></div></div>
			</div>
			<div class="ult-column col-md-8 " id="col-4-2">
				<div class="colwrapper"><div class="widget widget_ultimatummenu inner-container"><script type="text/javascript">
		//<![CDATA[
		jQuery(document).ready(function() {
			for (i=0;i<1;i++) {
				setTimeout('addDot()',1000);
				}
			});
			function addDot() {
				jQuery(document).ready(function($) {
					jQuery('#ultimatummenu-3-item .menu').wfMegaMenu({
						rowItems: 1,
						speed: 'normal',
						effect: 'slide',
						subMenuWidth:200
					});
				});
		}
		//]]>
    </script>
    
    <div class="ultimatum-nav">

        <div class="wfm-mega-menu" id="ultimatummenu-3-item" style="width:500px;float:right">
            <ul id="menu-main-menu" class="menu"><li id="menu-item-326" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-326"><a href="<?php echo get_site_url();?>">HOME</a></li>
                <li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="<?php echo get_site_url();?>/about">ABOUT US</a></li>
                <li id="menu-item-41" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a href="<?php echo get_site_url();?>/services">SERVICES</a></li>
                <li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a href="<?php echo get_site_url();?>/contact-page">CONTACT US</a></li>
            </ul>
        </div>

    </div>

	</div><div class="clearfix"></div></div>
			</div>
		</div></div>
	</div>
	<div class="ult-wrapper wrapper  hidden-desktop" id="wrapper-32">

	<div class="ult-container  container " id="container-32">
	<div class="row">
			<div class="ult-column col-md-9 " id="col-32-1">
				<div class="colwrapper"><div class="widget widget_ultimatummenu inner-container"><nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-ultimatummenu-7">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
													<a class="navbar-brand" href="<?php echo get_site_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ocean-drywall-new.png" title="Drywall Contactors - Ocean Drywall & Renovation"></a>
										</div>
			<div class="collapse navbar-collapse" id="bs-navbar-collapse-ultimatummenu-7">
				<ul id="menu-main-menu-1" class="nav navbar-nav"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-326 active"><a title="HOME" href="<?php echo get_site_url();?>">HOME</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a title="ABOUT US" href="<?php echo get_site_url();?>/about">ABOUT US</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a title="SERVICES" href="<?php echo get_site_url();?>/services">SERVICES</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a title="CONTACT US" href="<?php echo get_site_url();?>/contact-page">CONTACT US</a></li>
	</ul>                    </div>
		</div>
	</nav>
	</div><div class="clearfix"></div></div>
			</div>
			<div class="ult-column col-md-3 " id="col-32-2">
				<div class="colwrapper"><div class="widget widget_ultimatumwysiwyg inner-container"><div class="alignright"><a href="tel:6046142486"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone.png" alt="phone" width="25" height="25" class="size-full wp-image-211"></a></div>
	</div></div>
			</div>
			</div></div>
	</div>
</header>



<div class="contact-form-container">
    <div class="ult-wrapper wrapper">
        <div class="ult-container  container " id="container-4">
            <div class="row">

				<div class="ult-column col-md-6 ">
                    <div class="contact-form-wrapper">
                        <?php echo do_shortcode( '[contact-form-7 id="15" title="Contact form 1"]' ); ?>
                    </div>
                </div>

                <div class="ult-column col-md-6 ">
                    <div class="contact-form-wrapper map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1302.9408657375482!2d-122.99850044174943!3d49.221769994831156!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5486765cbf49c0c1%3A0xfd137c73fd4e883!2s6818%20Waverley%20Ave%2C%20Burnaby%2C%20BC%20V5J%204A5!5e0!3m2!1sen!2sca!4v1567918845219!5m2!1sen!2sca" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="sidebar-instagram">
	<?php get_sidebar(); ?>		
</div>


<footer class="footwrapper">
<div class="ult-wrapper wrapper " id="wrapper-11">

<div class="ult-container  container " id="container-11">
<div class="row">
		<div class="ult-column col-md-12 " id="col-11-1">
			<div class="colwrapper"><div class="widget widget_ultimatummenu inner-container"><div class="ultimatum-menu-container" data-menureplacer="480"><div class="ultimatum-regular-menu"><script type="text/javascript">
	//<![CDATA[
	jQuery(document).ready(function() {
		for (i=0;i<1;i++) {
			setTimeout('addDot()',1000);
			}
		});
		function addDot() {
			jQuery(document).ready(function($) {
				jQuery('#ultimatummenu-5-item .menu').wfMegaMenu({
					rowItems: 1,
					speed: 'normal',
					effect: 'slide',
					subMenuWidth:200
				});
			});
	}
	//]]>
</script>
<div class="ultimatum-nav">
<div class="wfm-mega-menu" id="ultimatummenu-5-item" style="width:1170px;">
<ul id="menu-footer-menu" class="menu"><li id="menu-item-325" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-325"><a href="<?php echo get_site_url();?>">Home</a></li>
<li id="menu-item-158" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="<?php echo get_site_url();?>/about">About Us</a></li>
<li id="menu-item-157" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157"><a href="<?php echo get_site_url();?>/contact-page">Contact Us</a></li>
<li id="menu-item-155" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-39 current_page_item menu-item-155"><a href="<?php echo get_site_url();?>/services">Services</a></li>
</ul></div>
</div></div><style>
    #ultimatummenu-5-responsive-menu .slicknav_btn {float:left;?>}
</style>
<div style="display:none">
    <ul id="ultimatummenu-5-resonsive" class="menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-325"><a href="index.hml">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="<?php echo get_site_url();?>/about">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157"><a href="<?php echo get_site_url();?>">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-39 current_page_item menu-item-155"><a href="<?php echo get_site_url();?>/services">Services</a></li>
</ul></div>
<div id="ultimatummenu-5-responsive-menu" class="ultimatum-responsive-menu" style="display: none;"><div class="slicknav_menu" style="display: none;"><a href="#" aria-haspopup="true" tabindex="0" class="slicknav_btn slicknav_collapsed" style="outline: none;"><span class="slicknav_menutxt">Menu</span><span class="slicknav_icon"><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span></span></a><ul class="slicknav_nav slicknav_hidden" aria-hidden="true" role="menu" style="display: none;"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-325"><a href="index.hml" role="menuitem" tabindex="-1">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="<?php echo get_site_url();?>/about" role="menuitem" tabindex="-1">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157"><a href="<?php echo get_site_url();?>" role="menuitem" tabindex="-1">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-39 current_page_item menu-item-155"><a href="<?php echo get_site_url();?>/services" role="menuitem" tabindex="-1">Services</a></li>
</ul></div>
 <div class="slicknav_menu" style="display: none;"></div>
</div>
<script type="text/javascript">
	//<![CDATA[
	jQuery(document).ready(function() {
	    jQuery('#ultimatummenu-5-resonsive').slicknav({
            label:'Menu',
            allowParentLinks: true,            prependTo:'#ultimatummenu-5-responsive-menu'
        });
	});
//]]>
</script></div></div><div class="clearfix"></div></div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper  hidden-phone hidden-tablet hidden-desktop" id="wrapper-12">

<div class="ult-container  container " id="container-12">
<div class="row">
		<div class="ult-column col-md-12 " id="col-12-1">
			<div class="colwrapper">&nbsp;</div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper " id="wrapper-34">

<div class="ult-container  container " id="container-34">
<div class="row">
		<div class="ult-column col-md-12 " id="col-34-1">
			<div class="colwrapper"><div class="widget widget_ultimatumwysiwyg inner-container"><div class="section group">
<div class="col span_1_of_3">
<div id="uxi_widget_wysiwyg_text_area-10" class="uxi-widget" data-column="1" data-row="22">
<div class="content">
<p><strong><span style="font-size:16px; color:#8b8e91;">CALL US:</span> <span style="color:#ffffff; font-size:22px;">604-614-2486</span></strong><br>
<strong><span style="font-size:16px; color:#8b8e91;">TOP RATED DRYWALL COMPANY</span></strong></p>
</div>
</div>
</div>
<div class="col span_1_of_3" style=" text-align: center;"><span style="color:#ffffff;">© 2019 Ocean Drywall. All Rights Reserved.</span></div>
<div class="col span_1_of_3">
<div style="float: right; text-align: center; width: 100%; max-width: 300px;"><a id="button-id-1" class="button button-34 button-block" href="<?php echo get_site_url();?>/contact-page"><span class="button-inner"><span class="button-text-wrap"><span class="button-text sub-header-font">FREE PROJECT QUOTE</span></span></span></a></div>
</div>
</div>
</div></div>
		</div>
		</div></div>
</div>
</footer>
<!-- Lightbox Plus Colorbox v2.7.2/1.5.9 - 2013.01.24 - Message: 0-->
<script type="text/javascript">
jQuery(document).ready(function($){
  $("a[rel*=lightbox]").colorbox({initialWidth:"30%",initialHeight:"30%",maxWidth:"90%",maxHeight:"90%",opacity:0.8});
});



<!--Generated by Endurance Page Cache--><div id="cboxOverlay" style="display: none;"></div><div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;"><div id="cboxWrapper"><div><div id="cboxTopLeft" style="float: left;"></div><div id="cboxTopCenter" style="float: left;"></div><div id="cboxTopRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxMiddleLeft" style="float: left;"></div><div id="cboxContent" style="float: left;"><div id="cboxTitle" style="float: left;"></div><div id="cboxCurrent" style="float: left;"></div><button type="button" id="cboxPrevious"></button><button type="button" id="cboxNext"></button><button id="cboxSlideshow"></button><div id="cboxLoadingOverlay" style="float: left;"></div><div id="cboxLoadingGraphic" style="float: left;"></div></div><div id="cboxMiddleRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxBottomLeft" style="float: left;"></div><div id="cboxBottomCenter" style="float: left;"></div><div id="cboxBottomRight" style="float: left;"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div></div><div id="cboxOverlay" style="display: none;"></div><div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;"><div id="cboxWrapper"><div><div id="cboxTopLeft" style="float: left;"></div><div id="cboxTopCenter" style="float: left;"></div><div id="cboxTopRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxMiddleLeft" style="float: left;"></div><div id="cboxContent" style="float: left;"><div id="cboxTitle" style="float: left;"></div><div id="cboxCurrent" style="float: left;"></div><button type="button" id="cboxPrevious"></button><button type="button" id="cboxNext"></button><button id="cboxSlideshow"></button><div id="cboxLoadingOverlay" style="float: left;"></div><div id="cboxLoadingGraphic" style="float: left;"></div></div><div id="cboxMiddleRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxBottomLeft" style="float: left;"></div><div id="cboxBottomCenter" style="float: left;"></div><div id="cboxBottomRight" style="float: left;"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div></div></body>


<?php
    get_footer();
?>