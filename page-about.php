<?php
/**
* Template Name: About
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header();

?>

<body class="page-template-default page page-id-39 wpb-js-composer js-comp-ver-4.7.2a vc_responsive  ut-tbs3-default ut-layout-full-width-layout-page" data-gr-c-s-loaded="true">
<div class="clear"></div>

<header class="headwrapper">
	<div class="ult-wrapper wrapper  hidden-phone hidden-tablet" id="wrapper-1">

	<div class="ult-container  container " id="container-1">
	<div class="row">
			<div class="ult-column col-md-12 " id="col-1-1">
				<div class="colwrapper"><div class="widget widget_ultimatumwysiwyg inner-container"><div id="jumbotron2-11" class="col span_2_of_3">
	<h2 class="jumbotron-heading sub-header-font"><span class="jumbotron-heading-inner">Drywall Experts You can Count on</span></h2>
	</div>
	<div class="col span_1_of_3" style="float: right; margin-top: 5px;">
	<div style="float: right; text-align: center; width: 100%; max-width: 300px; min-width: 280px;"><a id="button-id-1" class="button button-34 button-block" href="tel:6046142486"><span class="button-inner"><span class="button-text-wrap"><span class="button-text sub-header-font" style="margin-bottom: -5px;"><span style="font-size: 24px">Tel: 604-614-2486</span></span></span></span></a></div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>

	<div class="ult-wrapper wrapper  hidden-phone hidden-tablet" id="wrapper-4">

	<div class="ult-container  container " id="container-4">
	<div class="row">
			<div class="ult-column col-md-4 " id="col-4-1">
				<div class="colwrapper"><div id="logo-container"><h2><a href="<?php echo get_site_url();?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ocean-drywall-new.png" alt="Drywall Contactors - Ocean Drywall & Renovation" class="img-responsive"></a></h2></div></div>
			</div>
			<div class="ult-column col-md-8 " id="col-4-2">
				<div class="colwrapper"><div class="widget widget_ultimatummenu inner-container"><script type="text/javascript">
		//<![CDATA[
		jQuery(document).ready(function() {
			for (i=0;i<1;i++) {
				setTimeout('addDot()',1000);
				}
			});
			function addDot() {
				jQuery(document).ready(function($) {
					jQuery('#ultimatummenu-3-item .menu').wfMegaMenu({
						rowItems: 1,
						speed: 'normal',
						effect: 'slide',
						subMenuWidth:200
					});
				});
		}
		//]]>
	</script>
		<div class="ultimatum-nav">

		<div class="wfm-mega-menu" id="ultimatummenu-3-item" style="width:500px;float:right">
			<ul id="menu-main-menu" class="menu"><li id="menu-item-326" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-326"><a href="<?php echo get_site_url();?>">HOME</a></li>
				<li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="<?php echo get_site_url();?>/about">ABOUT US</a></li>
				<li id="menu-item-41" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a href="<?php echo get_site_url();?>/services">SERVICES</a></li>
				<li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a href="<?php echo get_site_url();?>/contact-page">CONTACT US</a></li>
			</ul>
		</div>

		</div>

	</div><div class="clearfix"></div></div>
			</div>
		</div></div>
	</div>
	<div class="ult-wrapper wrapper  hidden-desktop" id="wrapper-32">

	<div class="ult-container  container " id="container-32">
	<div class="row">
			<div class="ult-column col-md-9 " id="col-32-1">
				<div class="colwrapper"><div class="widget widget_ultimatummenu inner-container"><nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-ultimatummenu-7">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
													<a class="navbar-brand" href="<?php echo get_site_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ocean-drywall-new.png" title="Drywall Contactors - Ocean Drywall & Renovation"></a>
										</div>
			<div class="collapse navbar-collapse" id="bs-navbar-collapse-ultimatummenu-7">
				<ul id="menu-main-menu-1" class="nav navbar-nav"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-326 active"><a title="HOME" href="<?php echo get_site_url();?>">HOME</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a title="ABOUT US" href="<?php echo get_site_url();?>/about">ABOUT US</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a title="SERVICES" href="<?php echo get_site_url();?>/services">SERVICES</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a title="CONTACT US" href="<?php echo get_site_url();?>/contact-page">CONTACT US</a></li>
	</ul>                    </div>
		</div>
	</nav>
	</div><div class="clearfix"></div></div>
			</div>
			<div class="ult-column col-md-3 " id="col-32-2">
				<div class="colwrapper"><div class="widget widget_ultimatumwysiwyg inner-container"><div class="alignright"><a href="tel:6046142486"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone.png" alt="phone" width="25" height="25" class="size-full wp-image-211"></a></div>
	</div></div>
			</div>
			</div></div>
	</div>
</header>

<div class="bodywrapper" id="bodywrapper">
<div class="ult-wrapper wrapper " id="wrapper-13">

<div class="ult-container  container " id="container-13">
<div class="row">
		<div class="ult-column col-md-12 " id="col-13-1">
			<div class="colwrapper"><div class="widget widget_ultimatumpcontent inner-container"><div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-parallax="1.5" class="vc_row wpb_row vc_row-fluid vc_custom_1443827758558 vc_general vc_parallax vc_parallax-content-moving" style="position: relative; left: -355px; box-sizing: border-box; width: 1920px; padding-left: 355px; padding-right: 355px;"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper"><h1 style="color: #ffffff;text-align: center" class="vc_custom_heading vc_custom_1444078773947">DRYWALL SERVICE</h1><h2 style="font-size: 24px;color: #1da1f2;text-align: center" class="vc_custom_heading vc_custom_1444079341471">Reliable, Affordable, Reasonable</h2><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper"><div style="display: flex; justify-content: center;"><a class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-flat vc_btn3-color-warning" href="<?php echo get_site_url();?>/contact-page" title="" target="_self">CONTACT US TODAY!</a></div>
</div></div></div></div></div><div class="vc_parallax-inner skrollable skrollable-between" data-bottom-top="top: -50%;" data-top-bottom="top: 0%;" style="height: 150%; top: -29.0943%;"></div></div><div class="vc_row-full-width"></div>
</div></div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper " id="wrapper-14">

<div class="ult-container  container " id="container-14">
<div class="row">
		<div class="ult-column col-md-12 " id="col-14-1">
			<div class="colwrapper"><div class="inner-container"> 	<div id="content" role="main">
 	 	 	<article id="post-39" class="post-39 page type-page status-publish hentry entry post-inner">

 		 		 		 
 		 		<div class="entry-content">
 			
<h1>Learn About Your Top Rated Drywall Contractors</h1>
<p>We are passionate about what we do.</p>
<p>That’s the Ocean Drywall & Renovation difference.</p>
<p>For years, we have been researching, working in and perfecting our craft so that we can come to you with the knowledge, expertise and detail-oriented mindset which sets us apart from your average professional drywall company. We knew when we started that we wanted to stand above the rest, to go the extra mile for every client, every day. To do this, we knew we had to be so much more than drywall professionals – we had to be craftsmen, artisans, and a customer-service-driven team bent on solving old problems in new ways, under budget and on time.</p>
<p>In this way, we are able to bring a different kind of drywall service to your door, whether that door is to your home, your business, or your industrial complex. Whether you need a new construction drywall expert or you are looking to do simple renovations, we are the experts who can get you the results you are looking for, and our work is designed to last.</p>
<p>Because our drywall contractors are driven by our customers’ experience with us, we work with you, every step of the way, really getting to know you and your space on a personal level so that we can work to build walls and client relationships that last a lifetime. We pride ourselves on our ability to provide both excellent service and quality work for some of the most competitive prices on the market.</p>
<p>Whether you are looking for a drywall contractor to do drywall repairs, installations or need water damage restoration in your home or business, we are just the team for you. We will provide you with friendly and quality service that you can depend on.</p>
<p>Contact us today to learn more about what we can do for you, and see why Vancouver Island customers love our drywall services. It’s time for you to sit back, relax, and trust the professionals who do every job with precision. Call now!</p>

<h3>SOME SERVICES INCLUDE:</h3>
<p>Drywall Tape, Drywall Repair, Drywall Mud, Sheetrock, Wallboard, Drywalling, Gypsum Board, Drywall Installation, Drywall Contractors, The Soundproof Drywall, Ceiling Texture, Drywall Patch, Drywall Ceiling, Bathroom Drywall, Drywall Companies, Drywall Calculator, Drywall Finishing, Drywall Plaster, Hanging Drywall, Drywall Jobs, Drywall Services, Hanging Drywall, Fixing Drywall, Installing Drywall Ceiling, Basement Drywall, Gypsum Drywall, Drywall Estimator, Drywall Estimate, Drywall Estimates, Drywall Garage, Ceiling Drywall Repair, Drywall Patching, Sheetrock Drywall, Drywall Crack Repair, Drywall Board, Fixing Drywall Cracks, Drywall Texture, Drywall Studs, Insulation, Foam Insulation, Attic Insulation, Home Insulation, Wall Insulation, Spray Foam, Insulated, Soundproofing, Acoustic Panels, Soundproof Room, Sound Barrier, Sound Proof Walls, Soundproof Drywall, Sound Insulation, Soundproof Ceiling, Sound Absorbing Panels, Sound Deadening, Acoustic Insulation, Soundproofing Basement Ceiling, Acoustic Wall Panels, Soundproof Basement, Sound Proof Insulation, Sound Isolation, Batt Insulation, Fiberglas Insulation, Rockwool Insulation, Styrofoam Insulation, Insulating Basement Walls, Foam Board Insulation, Sound Insulation, Pink Insulation, Mineral Wool, Insulation Contractors, Mineral Wool Insulation, Eco Insulation, Thermal Insulation, Insulation Installation, Insulation Material, Polystyrene Insulation, Acoustic Insulation, Exterior Wall Insulation, Ceiling Insulation, Steel Studs, Metal Studs, Stud Wall, Steel Stud Framing, Metal Frame, Wood Stud, Metal Stud, Framing, Steel Framing, Drywall Studs, Metal Stud Wall, Stud Construction, Framing Studs etc.</p>

</div>

 		</article></div>
 		 		<div class="clearfix"></div>
 	
 	<div class="clearfix"></div><div style="clear:both"></div> 	</div>
 	</div></div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper " id="wrapper-17">

<div class="ult-container  container " id="container-17">
<div class="row">
		<div class="ult-column col-md-12 " id="col-17-1">
			<div class="colwrapper">&nbsp;</div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper " id="wrapper-18">

<div class="ult-container  container " id="container-18">
<div class="row">
		<div class="ult-column col-md-12 " id="col-18-1">
			<div class="colwrapper">&nbsp;</div>
		</div>
		</div></div>
</div>

<div class="sidebar-instagram">
	<?php get_sidebar(); ?>		
</div>

<footer class="footwrapper">
<div class="ult-wrapper wrapper " id="wrapper-11">

<div class="ult-container  container " id="container-11">
<div class="row">
		<div class="ult-column col-md-12 " id="col-11-1">
			<div class="colwrapper"><div class="widget widget_ultimatummenu inner-container"><div class="ultimatum-menu-container" data-menureplacer="480"><div class="ultimatum-regular-menu"><script type="text/javascript">
	//<![CDATA[
	jQuery(document).ready(function() {
		for (i=0;i<1;i++) {
			setTimeout('addDot()',1000);
			}
		});
		function addDot() {
			jQuery(document).ready(function($) {
				jQuery('#ultimatummenu-5-item .menu').wfMegaMenu({
					rowItems: 1,
					speed: 'normal',
					effect: 'slide',
					subMenuWidth:200
				});
			});
	}
	//]]>
</script>
<div class="ultimatum-nav">
<div class="wfm-mega-menu" id="ultimatummenu-5-item" style="width:1170px;">
<ul id="menu-footer-menu" class="menu"><li id="menu-item-325" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-325"><a href="<?php echo get_site_url();?>">Home</a></li>
<li id="menu-item-158" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="<?php echo get_site_url();?>/about">About Us</a></li>
<li id="menu-item-157" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157"><a href="<?php echo get_site_url();?>/contact-page">Contact Us</a></li>

<li id="menu-item-155" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-39 current_page_item menu-item-155"><a href="<?php echo get_site_url();?>/services">Services</a></li>
</ul></div>
</div></div><style>
    #ultimatummenu-5-responsive-menu .slicknav_btn {float:left;?>}
</style>
<div style="display:none">
    <ul id="ultimatummenu-5-resonsive" class="menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-325"><a href="index.hml">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="<?php echo get_site_url();?>/about">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157"><a href="<?php echo get_site_url();?>">Contact Us</a></li>

<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-39 current_page_item menu-item-155"><a href="<?php echo get_site_url();?>/services">Services</a></li>
</ul></div>
<div id="ultimatummenu-5-responsive-menu" class="ultimatum-responsive-menu" style="display: none;"><div class="slicknav_menu" style="display: none;"><a href="#" aria-haspopup="true" tabindex="0" class="slicknav_btn slicknav_collapsed" style="outline: none;"><span class="slicknav_menutxt">Menu</span><span class="slicknav_icon"><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span></span></a><ul class="slicknav_nav slicknav_hidden" aria-hidden="true" role="menu" style="display: none;"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-325"><a href="index.hml" role="menuitem" tabindex="-1">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="<?php echo get_site_url();?>/about" role="menuitem" tabindex="-1">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157"><a href="<?php echo get_site_url();?>" role="menuitem" tabindex="-1">Contact Us</a></li>

<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-39 current_page_item menu-item-155"><a href="<?php echo get_site_url();?>/services" role="menuitem" tabindex="-1">Services</a></li>
</ul></div>
 <div class="slicknav_menu" style="display: none;"></div>
</div>
<script type="text/javascript">
	//<![CDATA[
	jQuery(document).ready(function() {
	    jQuery('#ultimatummenu-5-resonsive').slicknav({
            label:'Menu',
            allowParentLinks: true,            prependTo:'#ultimatummenu-5-responsive-menu'
        });
	});
//]]>
</script></div></div><div class="clearfix"></div></div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper  hidden-phone hidden-tablet hidden-desktop" id="wrapper-12">

<div class="ult-container  container " id="container-12">
<div class="row">
		<div class="ult-column col-md-12 " id="col-12-1">
			<div class="colwrapper">&nbsp;</div>
		</div>
		</div></div>
</div>
<div class="ult-wrapper wrapper " id="wrapper-34">

<div class="ult-container  container " id="container-34">
<div class="row">
		<div class="ult-column col-md-12 " id="col-34-1">
			<div class="colwrapper"><div class="widget widget_ultimatumwysiwyg inner-container"><div class="section group">
<div class="col span_1_of_3">
<div id="uxi_widget_wysiwyg_text_area-10" class="uxi-widget" data-column="1" data-row="22">
<div class="content">
<p><strong><span style="font-size:16px; color:#8b8e91;">CALL US:</span> <span style="color:#ffffff; font-size:22px;">604-614-2486</span></strong><br>
<strong><span style="font-size:16px; color:#8b8e91;">TOP RATED DRYWALL COMPANY</span></strong></p>
</div>
</div>
</div>
<div class="col span_1_of_3" style=" text-align: center;"><span style="color:#ffffff;">© 2019 Ocean Drywall. All Rights Reserved.</span></div>
<div class="col span_1_of_3">
<div style="float: right; text-align: center; width: 100%; max-width: 300px;"><a id="button-id-1" class="button button-34 button-block" href="<?php echo get_site_url();?>/contact-page"><span class="button-inner"><span class="button-text-wrap"><span class="button-text sub-header-font">FREE PROJECT QUOTE</span></span></span></a></div>
</div>
</div>
</div></div>
		</div>
		</div></div>
</div>
</footer>
<!-- Lightbox Plus Colorbox v2.7.2/1.5.9 - 2013.01.24 - Message: 0-->
<script type="text/javascript">
jQuery(document).ready(function($){
  $("a[rel*=lightbox]").colorbox({initialWidth:"30%",initialHeight:"30%",maxWidth:"90%",maxHeight:"90%",opacity:0.8});
});



<!--Generated by Endurance Page Cache--><div id="cboxOverlay" style="display: none;"></div><div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;"><div id="cboxWrapper"><div><div id="cboxTopLeft" style="float: left;"></div><div id="cboxTopCenter" style="float: left;"></div><div id="cboxTopRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxMiddleLeft" style="float: left;"></div><div id="cboxContent" style="float: left;"><div id="cboxTitle" style="float: left;"></div><div id="cboxCurrent" style="float: left;"></div><button type="button" id="cboxPrevious"></button><button type="button" id="cboxNext"></button><button id="cboxSlideshow"></button><div id="cboxLoadingOverlay" style="float: left;"></div><div id="cboxLoadingGraphic" style="float: left;"></div></div><div id="cboxMiddleRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxBottomLeft" style="float: left;"></div><div id="cboxBottomCenter" style="float: left;"></div><div id="cboxBottomRight" style="float: left;"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div></div><div id="cboxOverlay" style="display: none;"></div><div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;"><div id="cboxWrapper"><div><div id="cboxTopLeft" style="float: left;"></div><div id="cboxTopCenter" style="float: left;"></div><div id="cboxTopRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxMiddleLeft" style="float: left;"></div><div id="cboxContent" style="float: left;"><div id="cboxTitle" style="float: left;"></div><div id="cboxCurrent" style="float: left;"></div><button type="button" id="cboxPrevious"></button><button type="button" id="cboxNext"></button><button id="cboxSlideshow"></button><div id="cboxLoadingOverlay" style="float: left;"></div><div id="cboxLoadingGraphic" style="float: left;"></div></div><div id="cboxMiddleRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxBottomLeft" style="float: left;"></div><div id="cboxBottomCenter" style="float: left;"></div><div id="cboxBottomRight" style="float: left;"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div></div></body>



<?php
    get_footer();
?>
